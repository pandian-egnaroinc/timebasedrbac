name := """Cal"""
organization := "com.egnaro"
maintainer := "pandian@egnaroinc.com"
version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies += guice
