package timeBasedRbac;

import java.time.*;
import java.io.Serializable;

/**
 * The Rbac date time config.
 * The the Rbac permissions are valid only for the duration specified by this config.
 */
public class RbacDateTimeConfig implements Serializable {
    private LocalDate startDate;
    private Boolean allDay;
    private LocalTime startTime;
    private LocalTime endTime;
    private Boolean recurring;
    private RbacRecurrenceConfig rbacRecurrenceConfig;

    /**
     * Default Constructor.
     */
    public RbacDateTimeConfig() {
        this.startDate = null;
        this.allDay = true;
        this.startTime = null;
        this.endTime = null;
        this.recurring = false;
        this.rbacRecurrenceConfig = null;
    }

    /**
     * Setter for start date config.
     * @param startDate the start date config.
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * Setter for all day config.
     * @param allDay the all day config.
     */
    public void setAllDay(Boolean allDay) {
        this.allDay = allDay;
    }

    /**
     * Setter for start time config.
     * @param startTime the start time config.
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Setter for end time config.
     * @param endTime the end time config.
     */
    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Setter for rbac recurrence config.
     * @param rbacRecurrenceConfig the rbac recurrence config.
     */
    public void setRbacRecurrenceConfig(RbacRecurrenceConfig rbacRecurrenceConfig) {
        this.rbacRecurrenceConfig = rbacRecurrenceConfig;
    }

    /**
     * Setter for recurring config.
     * @param recurring the recurring config.
     */
    public void setRecurring(Boolean recurring) {
        this.recurring = recurring;
    }

    /**
     * Getter for start date config.
     * @return the start date config.
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * Getter for all day config.
     * @return the all day config.
     */
    public Boolean isAllDay() {
        return allDay;
    }

    /**
     * Getter for start time config.
     * @return the start time config.
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Getter for end time config.
     * @return the end time config.
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * Getter for rbac recurrence config.
     * @return the rbac recurrence config.
     */
    public RbacRecurrenceConfig getRbacRecurrenceConfig() {
        return rbacRecurrenceConfig;
    }

    /**
     * Getter for recurring config.
     * @return the recurring config.
     */
    public Boolean isRecurring() {
        return recurring;
    }

    /**
     * Validate Rbac date time config.
     * @return true is valid.
     */
    public String validate() {
        if(this.startDate == null) {
            return "Start date invalid.";
        }

        if(isAllDay() == false) {
            if(startTime == null || endTime == null) {
                return "Either 'allDay' is invalid or startTime and EndTime are invalid.";
            }
        }

        if(isRecurring() == true) {
            if(rbacRecurrenceConfig == null) {
                return "Either 'recurring' is invalid or RbacRecurrenceConfig is invalid";
            }
            try {
                rbacRecurrenceConfig.validate();
            } catch (RuntimeException e) {
                return "RbacRecurrenceConfig is invalid - " + e.getMessage();
            }
        }

        return "valid";
    }
}
