package timeBasedRbac;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * The rbac recurrence configuration.
 * Should be serializable.
 */
public class RbacRecurrenceConfig implements Serializable {
    private int repeatEvery;
    private RbacRecurrenceType rbacRecurrenceType;
    private Boolean neverEnding;
    private LocalDate endDate;
    private int recurrenceCount;
    private Set<DayOfWeek> daysOfWeek;

    /**
     * Default constructor.
     */
    public RbacRecurrenceConfig() {
        this.repeatEvery = -1;
        this.rbacRecurrenceType = RbacRecurrenceType.NONE;
        this.neverEnding = true;
        this.endDate = null;
        this.recurrenceCount = -1;
        this.daysOfWeek = new HashSet<>();
    }

    /**
     * Setter for repeat every config.
     * @param repeatEvery the repeat every config.
     */
    public void setRepeatEvery(int repeatEvery) {
        this.repeatEvery = repeatEvery;
    }

    /**
     * Setter for recurrence type config.
     * @param rbacRecurrenceType the recurrence type config.
     */
    public void setRbacRecurrenceType(RbacRecurrenceType rbacRecurrenceType) {
        this.rbacRecurrenceType = rbacRecurrenceType;
    }

    /**
     * Setter for end date config.
     * @param endDate the end date config.
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * Setter for recurrenceCount config.
     * @param recurrenceCount the recurrenceCount config.
     */
    public void setRecurrenceCount(int recurrenceCount) {
        this.recurrenceCount = recurrenceCount;
    }

    /**
     * Setter for neverEnding config.
     * @param neverEnding the neverEnding config.
     */
    public void setNeverEnding(Boolean neverEnding) {
        this.neverEnding = neverEnding;
    }

    /**
     * Setter for daysOfWeek config.
     * @param daysOfWeek the daysOfWeek config.
     */
    public void setDaysOfWeek(Set<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    /**
     * Getter for repeat every config.
     * @return the repeat every config.
     */
    public int getRepeatEvery() {
        return repeatEvery;
    }

    /**
     * Getter for recurrence type config.
     * @return the recurrence type config.
     */
    public RbacRecurrenceType getRbacRecurrenceType() {
        return rbacRecurrenceType;
    }

    /**
     * Getter for end date config.
     * @return the end date config.
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * Getter for recurrenceCount config.
     * @return the recurrenceCount config.
     */
    public int getRecurrenceCount() {
        return recurrenceCount;
    }

    /**
     * Getter for neverEnding config.
     * @return the neverEnding config.
     */
    public Boolean isNeverEnding() {
        return neverEnding;
    }

    /**
     * Getter for daysOfWeek config.
     * @return the daysOfWeek config.
     */
    public Set<DayOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    /**
     * Validates if the given configuration is correct or throws exception otherwise.
     * @return this if all the validations succeed.
     */
    public RbacRecurrenceConfig validate() {
        if(this.repeatEvery < 1)
            throw new RuntimeException ("repeatEvery value invalid.");

        if(this.rbacRecurrenceType == RbacRecurrenceType.NONE)
            throw new RuntimeException("recurrenceType invalid");

        if(this.rbacRecurrenceType == RbacRecurrenceType.WEEKS && (this.getDaysOfWeek() == null || this.getDaysOfWeek().isEmpty()))
            throw new RuntimeException("Weekly recurrence config invalid.");

        if(!this.neverEnding && this.endDate == LocalDate.MAX && this.recurrenceCount < 1)
            throw new RuntimeException("endDate or recurrenceCount invalid 1");

        if(this.neverEnding && !(this.endDate == LocalDate.MAX || this.recurrenceCount == -1))
            throw new RuntimeException("endDate or recurrenceCount invalid 2");

        //All validations succeeded. Return this.
        return this;
    }
}
