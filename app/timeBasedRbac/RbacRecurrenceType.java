package timeBasedRbac;

/**
 * The recurrence type enum.
 */
public enum RbacRecurrenceType {
    DAYS,   /** Daily recurrence.   */
    WEEKS,  /** Weekly recurrence.  */
    MONTHS, /** Monthly recurrence. */
    YEARS,  /** Yearly recurrence.  */
    NONE    /** Invalid config.     */
}
