package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import timeBasedRbac.RbacDateTimeConfig;
import timeBasedRbac.RbacRecurrenceConfig;
import timeBasedRbac.RbacRecurrenceType;
import validator.TimeBasedRBACValidator;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedHashSet;
import java.util.Set;

public class ValidationController extends Controller {
    public Result validationTest() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(false);

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 9, 0, 0);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 1, 1, 19, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 2, 1, 0, 0, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 1, 9, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();


        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 2, 2));
        testConfig.setAllDay(false);
        testConfig.setRecurring(false);
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 2, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 2, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 1, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest2() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 3, 3));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 2, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 2, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 1, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest3() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 2, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2021, 12, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 1, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2018, 1, 1, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest4() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 12, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2019, 1, 1, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest5() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 12, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2022, 1, 1, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest6() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig3());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 12, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 2, 9, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 28, 10, 0, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest7() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig3());
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 12, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 1, 8, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 27, 18, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest8() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime negative1 = LocalDateTime.of(2020, 2, 1, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2020, 2, 2, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        LocalDateTime positive1 = LocalDateTime.of(2020, 2, 3, 8, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 2, 4, 18, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime positive3 = LocalDateTime.of(2020, 2, 5, 17, 30, 30);
        Boolean result5 = new TimeBasedRBACValidator(testConfigFromJson, positive3).validate();

        LocalDateTime positive4 = LocalDateTime.of(2020, 2, 6, 8, 10, 0);
        Boolean result6 = new TimeBasedRBACValidator(testConfigFromJson, positive4).validate();

        LocalDateTime positive5 = LocalDateTime.of(2020, 2, 7, 18, 30, 30);
        Boolean result7 = new TimeBasedRBACValidator(testConfigFromJson, positive5).validate();

        return ok("negative1 : " + result1 + " negative2 : " + result2 + " positive1 : " + result3 + " positive2 : " + result4
                            + " positive3 : " + result5 + " positive4 : " + result6 + " positive5 : " + result7);
    }

    public Result validationTest9() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 30, 30));
        testConfig.setEndTime(LocalTime.of(18, 30, 30));
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime negative1 = LocalDateTime.of(2020, 1, 2, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2020, 1, 15, 10, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        LocalDateTime negative3 = LocalDateTime.of(2020, 1, 12, 10, 30, 30);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative3).validate();

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 4, 9, 40, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 1, 19, 18, 30, 23);
        Boolean result5 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime positive3 = LocalDateTime.of(2020, 2, 15, 17, 30, 30);
        Boolean result6 = new TimeBasedRBACValidator(testConfigFromJson, positive3).validate();

        LocalDateTime positive4 = LocalDateTime.of(2020, 2, 16, 10, 10, 0);
        Boolean result7 = new TimeBasedRBACValidator(testConfigFromJson, positive4).validate();

        LocalDateTime positive5 = LocalDateTime.of(2020, 2, 2, 18, 30, 20);
        Boolean result8 = new TimeBasedRBACValidator(testConfigFromJson, positive5).validate();

        return ok("negative1 : " + result1 + " negative2 : " + result2 + " negative3 : " + result3 + " positive1 : " + result4
                + " positive2 : " + result5 + " positive3 : " + result6 + " positive4 : " + result7 + " positive5 : " + result8);
    }

    public Result validationTest10() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime negative1 = LocalDateTime.of(2020, 1, 4, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2020, 1, 5, 10, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        LocalDateTime negative3 = LocalDateTime.of(2020, 1, 12, 10, 30, 30);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative3).validate();

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 3, 9, 40, 0);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 2, 18, 18, 30, 23);
        Boolean result5 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime positive3 = LocalDateTime.of(2020, 3, 25, 17, 30, 30);
        Boolean result6 = new TimeBasedRBACValidator(testConfigFromJson, positive3).validate();

        LocalDateTime positive4 = LocalDateTime.of(2021, 2, 16, 10, 10, 0);
        Boolean result7 = new TimeBasedRBACValidator(testConfigFromJson, positive4).validate();

        LocalDateTime positive5 = LocalDateTime.of(2022, 2, 9, 18, 30, 20);
        Boolean result8 = new TimeBasedRBACValidator(testConfigFromJson, positive5).validate();

        return ok("negative1 : " + result1 + " negative2 : " + result2 + " negative3 : " + result3 + " positive1 : " + result4
                + " positive2 : " + result5 + " positive3 : " + result6 + " positive4 : " + result7 + " positive5 : " + result8);
    }

    public Result validationTest11() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 1, 1, 1);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2022, 2, 1, 23, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 10, 23, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2022, 2, 2, 11, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest12() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 1, 1, 1);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2020, 12, 1, 23, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 11, 2, 23, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2021, 1, 1, 11, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest13() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 1, 1, 1);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2021, 3, 1, 23, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2020, 2, 1, 23, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2020, 4, 1, 11, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest14() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 1, 1, 1);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2120, 1, 1, 23, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2019, 1, 1, 23, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2022, 1, 2, 11, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest15() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(8, 59, 59));
        testConfig.setEndTime(LocalTime.of(17, 59, 59));
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2029, 1, 1, 11, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2030, 1, 1, 8, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2022, 1, 2, 10, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    public Result validationTest16() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testConfigFromJson = Json.fromJson(jsonNode, RbacDateTimeConfig.class);

        LocalDateTime positive1 = LocalDateTime.of(2020, 1, 1, 10, 30, 30);
        Boolean result1 = new TimeBasedRBACValidator(testConfigFromJson, positive1).validate();

        LocalDateTime positive2 = LocalDateTime.of(2022, 1, 1, 17, 30, 30);
        Boolean result2 = new TimeBasedRBACValidator(testConfigFromJson, positive2).validate();

        LocalDateTime negative1 = LocalDateTime.of(2021, 1, 1, 8, 10, 0);
        Boolean result3 = new TimeBasedRBACValidator(testConfigFromJson, negative1).validate();

        LocalDateTime negative2 = LocalDateTime.of(2022, 1, 1, 18, 30, 30);
        Boolean result4 = new TimeBasedRBACValidator(testConfigFromJson, negative2).validate();

        return ok("positive1 : " + result1 + " positive2 : " + result2 + " negative1 : " + result3 + " negative2 : " + result4);
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(365);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2020,12,31));

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig3() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));
        rbacRecurrenceConfig.setDaysOfWeek(getWeekDays());

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(52);
        rbacRecurrenceConfig.setDaysOfWeek(getWeekEnds());

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(true);
        rbacRecurrenceConfig.setDaysOfWeek(getWeekDays());

        return rbacRecurrenceConfig.validate();
    }

    private Set<DayOfWeek> getWeekDays() {
        Set<DayOfWeek> weekDays = new LinkedHashSet<>();
        weekDays.add(DayOfWeek.MONDAY);
        weekDays.add(DayOfWeek.TUESDAY);
        weekDays.add(DayOfWeek.WEDNESDAY);
        weekDays.add(DayOfWeek.THURSDAY);
        weekDays.add(DayOfWeek.FRIDAY);

        return weekDays;
    }

    private Set<DayOfWeek> getWeekEnds() {
        Set<DayOfWeek> weekEnds = new LinkedHashSet<>();
        weekEnds.add(DayOfWeek.SATURDAY);
        weekEnds.add(DayOfWeek.SUNDAY);

        return weekEnds;
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(12);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(10);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2031,12,31));

        return rbacRecurrenceConfig.validate();
    }
}
