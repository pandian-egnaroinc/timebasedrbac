package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.*;
import timeBasedRbac.RbacDateTimeConfig;
import timeBasedRbac.RbacRecurrenceConfig;
import timeBasedRbac.RbacRecurrenceType;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class TestController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }

    public Result healthCheck() { return ok("Service is running."); }

    public Result parserTest() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(false);

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(false);
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }


    public Result parserTest_DailyRecurrence() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_DailyRecurrence1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_DailyRecurrence2() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_DailyRecurrence3() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createDailyRecurrenceConfig3());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_WeeklyRecurrence() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_WeeklyRecurrence1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 30, 30));
        testConfig.setEndTime(LocalTime.of(18, 30, 30));
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_WeeklyRecurrence2() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createWeeklyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_MonthlyRecurrence() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_MonthlyRecurrence1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 30, 30));
        testConfig.setEndTime(LocalTime.of(18, 30, 30));
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_MonthlyRecurrence2() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createMonthlyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_YearlyRecurrence() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(true);
        testConfig.setRecurring(true);
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_YearlyRecurrence1() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(8, 59, 59));
        testConfig.setEndTime(LocalTime.of(17, 59, 59));
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig1());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }

    public Result parserTest_YearlyRecurrence2() {
        RbacDateTimeConfig testConfig = new RbacDateTimeConfig();
        testConfig.setStartDate(LocalDate.of(2020, 1, 1));
        testConfig.setAllDay(false);
        testConfig.setRecurring(true);
        testConfig.setStartTime(LocalTime.of(9, 0, 0));
        testConfig.setEndTime(LocalTime.of(18, 0, 0));
        testConfig.setRbacRecurrenceConfig(createYearlyRecurrenceConfig2());

        JsonNode jsonNode = Json.toJson(testConfig);
        RbacDateTimeConfig testResult = Json.fromJson(jsonNode, RbacDateTimeConfig.class);
        return ok(Json.toJson(testResult).toString() + "\nValidity : " + testResult.validate());
    }



    private RbacRecurrenceConfig createDailyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(365);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2020,12,31));

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createDailyRecurrenceConfig3() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.DAYS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));
        rbacRecurrenceConfig.setDaysOfWeek(new HashSet<>());

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));
        rbacRecurrenceConfig.setDaysOfWeek(getWeekDays());

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(24);
        rbacRecurrenceConfig.setDaysOfWeek(getWeekEnds());

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createWeeklyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.WEEKS);
        rbacRecurrenceConfig.setNeverEnding(true);
        rbacRecurrenceConfig.setDaysOfWeek(getWeekDays());

        return rbacRecurrenceConfig.validate();
    }

    private Set<DayOfWeek> getWeekDays() {
        Set<DayOfWeek> weekDays = new LinkedHashSet<>();
        weekDays.add(DayOfWeek.MONDAY);
        weekDays.add(DayOfWeek.TUESDAY);
        weekDays.add(DayOfWeek.WEDNESDAY);
        weekDays.add(DayOfWeek.THURSDAY);
        weekDays.add(DayOfWeek.FRIDAY);

        return weekDays;
    }

    private Set<DayOfWeek> getWeekEnds() {
        Set<DayOfWeek> weekEnds = new LinkedHashSet<>();
        weekEnds.add(DayOfWeek.SATURDAY);
        weekEnds.add(DayOfWeek.SUNDAY);

        return weekEnds;
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(12);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createMonthlyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.MONTHS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2021,12,31));

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(true);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig1() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(1);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setRecurrenceCount(10);

        return rbacRecurrenceConfig.validate();
    }

    private RbacRecurrenceConfig createYearlyRecurrenceConfig2() {
        RbacRecurrenceConfig rbacRecurrenceConfig = new RbacRecurrenceConfig();
        rbacRecurrenceConfig.setRepeatEvery(2);
        rbacRecurrenceConfig.setRbacRecurrenceType(RbacRecurrenceType.YEARS);
        rbacRecurrenceConfig.setNeverEnding(false);
        rbacRecurrenceConfig.setEndDate(LocalDate.of(2031,12,31));

        return rbacRecurrenceConfig.validate();
    }
}
