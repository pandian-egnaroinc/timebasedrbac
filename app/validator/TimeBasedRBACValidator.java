package validator;

import timeBasedRbac.RbacDateTimeConfig;
import timeBasedRbac.RbacRecurrenceConfig;
import timeBasedRbac.RbacRecurrenceType;

import java.time.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Validator for time based RBAC.
 * Validates if the date-time passed is valid for the given time based RBAC configuration.
 */
public class TimeBasedRBACValidator {
    private final RbacDateTimeConfig rbacDateTimeConfig;
    private final LocalDateTime dateTimeToValidate;
    private ZoneId zoneId;

    /**
     * Constructor.
     * @param rbacDateTimeConfig the time based rbac configuration.
     * @param dateTimeToValidate the date time to test for validity.
     */
    public TimeBasedRBACValidator(RbacDateTimeConfig rbacDateTimeConfig, LocalDateTime dateTimeToValidate) {
        this.rbacDateTimeConfig = rbacDateTimeConfig;
        this.dateTimeToValidate = dateTimeToValidate;
    }

    /**
     * Constructor with ZoneId info.
     * @param rbacDateTimeConfig the time based rbac configuration.
     * @param dateTimeToValidate the date time to test for validity.
     * @param zoneId the zone id of the incoming request with timezone information.
     */
    public TimeBasedRBACValidator(RbacDateTimeConfig rbacDateTimeConfig, LocalDateTime dateTimeToValidate, ZoneId zoneId) {
        this.rbacDateTimeConfig = rbacDateTimeConfig;
        this.dateTimeToValidate = dateTimeToValidate;
        this.zoneId = zoneId;
    }

    /**
     * Tests the validity of the given date time is valid for the given RBAC configuration without considering Locale.
     * @return true if the given date time is valid for the given RBAC configuration.
     */
    public Boolean validate() {
        // Validate if the datetime to validate is overall within the valid date time range.
        if(dateTimeToValidate.isBefore(calculateStartDateTime(rbacDateTimeConfig)))
            return false;
        if(dateTimeToValidate.isAfter(calculateEndDateTime(rbacDateTimeConfig)))
            return false;

        //If it is a non recurring event, no more validations are needed. Return true.
        if(!rbacDateTimeConfig.isRecurring()) {
            return true;
        }

        //We have a recurring event. Validate based on recurrence type. Each type has different validations.
        RbacRecurrenceConfig rbacRecurrenceConfig = rbacDateTimeConfig.getRbacRecurrenceConfig().validate();
        RbacRecurrenceType rbacRecurrenceType = rbacRecurrenceConfig.getRbacRecurrenceType();

        //Validate Daily recurrence.
        if(rbacRecurrenceType == RbacRecurrenceType.DAYS) {
            return validateDailyRecurrence();
        }

        //Validate Weekly recurrence.
        if(rbacRecurrenceType == RbacRecurrenceType.WEEKS) {
            return validateWeeklyRecurrence();
        }

        //Validate Monthly recurrence.
        if(rbacRecurrenceType == RbacRecurrenceType.MONTHS) {
            return validateMonthlyRecurrence();
        }

        //Validate Yearly recurrence.
        if(rbacRecurrenceType == RbacRecurrenceType.YEARS) {
            return validateYearlyRecurrence();
        }

        //Should not reach here.
        return false;
    }

    /**
     * Tests the validity of the given date time is valid for the given RBAC configuration including Local timezone validation.
     * @return true if the given date time is valid for the given RBAC configuration.
     */
    public Boolean validateWithTimeZone() {
        ZonedDateTime zonedDateTimeToValidate = dateTimeToValidate.atZone(zoneId);
        return false;
    }

    /**
     * Validates if daily recurrence is valid.
     * @return true if validation is successful.
     */
    private Boolean validateDailyRecurrence() {
        LocalDate startDate = this.rbacDateTimeConfig.getStartDate();
        LocalDate dateToValidate = this.dateTimeToValidate.toLocalDate();

        LocalDate endDate = this.calculateEndDate(rbacDateTimeConfig);

        //Loop till you reach the validation date, incrementing by 'repeat every' days in each iteration.
        for(LocalDate currentDate = startDate;
                !currentDate.isAfter(dateToValidate) && !currentDate.isAfter(endDate);
                    currentDate = currentDate.plusDays(rbacDateTimeConfig.getRbacRecurrenceConfig().getRepeatEvery())) {

            if(currentDate.isEqual(dateToValidate)) {
                //Valid recurrence date reached. Validate time and return accordingly.
                return validateTime(currentDate);
            }
            //else loop through.
        }

        //Date to validate is not one of the recurrence dates.
        return false;
    }

    /**
     * Validates if an weekly recurrence is valid.
     * @return true if validation is successful.
     */
    private Boolean validateWeeklyRecurrence() {
        LocalDate startDate = this.rbacDateTimeConfig.getStartDate();
        LocalDate endDate = this.calculateEndDate(rbacDateTimeConfig);
        LocalDate dateToValidate = this.dateTimeToValidate.toLocalDate();
        Set<DayOfWeek> validDaysOfWeek = this.rbacDateTimeConfig.getRbacRecurrenceConfig().getDaysOfWeek();

        //Loop till you reach the validation date, incrementing by 'repeat every' weeks in each iteration.
        for(LocalDate currentDate = startDate;
                !currentDate.isAfter(dateToValidate.plusWeeks(1)) && !currentDate.isAfter(endDate);
                    currentDate = currentDate.plusWeeks(rbacDateTimeConfig.getRbacRecurrenceConfig().getRepeatEvery())) {

            Set<LocalDate> weekDays = getAllValidWeekdays(currentDate, validDaysOfWeek);
            if(weekDays.stream().anyMatch(weekDay -> weekDay.isEqual(dateToValidate))) {
                //One of the valid recurrence days reached. Validate time and return accordingly.
                return validateTime(dateToValidate);
            }
            //else loop through.
        }

        //Date to validate is not one of the recurrence dates.
        return false;
    }

    /**
     * Validates if an monthly recurrence is valid.
     * @return true if validation is successful.
     */
    private Boolean validateMonthlyRecurrence() {
        LocalDate startDate = this.rbacDateTimeConfig.getStartDate();
        LocalDate endDate = this.calculateEndDate(rbacDateTimeConfig);
        LocalDate dateToValidate = this.dateTimeToValidate.toLocalDate();

        //Loop till you reach the validation date, incrementing by 'repeat every' months in each iteration.
        for(LocalDate currentDate = startDate;
                !currentDate.isAfter(dateToValidate) && !currentDate.isAfter(endDate);
                    currentDate = currentDate.plusMonths(rbacDateTimeConfig.getRbacRecurrenceConfig().getRepeatEvery())) {

            if(currentDate.isEqual(dateToValidate)) {
                //Valid recurrence date reached. Validate time and return accordingly.
                return validateTime(currentDate);
            }
            //else loop through.
        }
        //Date to validate is not one of the recurrence dates.
        return false;
    }

    /**
     * Validates if an yearly recurrence is valid.
     * @return true if validation is successful.
     */
    private Boolean validateYearlyRecurrence() {
        LocalDate startDate = this.rbacDateTimeConfig.getStartDate();
        LocalDate endDate = this.calculateEndDate(rbacDateTimeConfig);
        LocalDate dateToValidate = this.dateTimeToValidate.toLocalDate();

        //Loop till you reach the validation date, incrementing by 'repeat every' years in each iteration.
        for(LocalDate currentDate = startDate;
                !currentDate.isAfter(dateToValidate) && !currentDate.isAfter(endDate);
                    currentDate = currentDate.plusYears(rbacDateTimeConfig.getRbacRecurrenceConfig().getRepeatEvery())) {

            if(currentDate.isEqual(dateToValidate)) {
                //Valid recurrence date reached. Validate time and return accordingly.
                return validateTime(currentDate);
            }
            //else loop through.
        }
        //Date to validate is not one of the recurrence dates.
        return false;
    }

    /**
     * Validates only if the time exists within the range.
     * Called after a successful date is validated.
     * @return true if the validation is successful.
     */
    private Boolean validateTime(LocalDate recurrenceDate) {
        //No time validation required for all day event.
        if(this.rbacDateTimeConfig.isAllDay()) {
            return true;
        }

        //Validate if the time is within the start and end range.
        if(this.dateTimeToValidate.isAfter(recurrenceDate.atTime(this.rbacDateTimeConfig.getStartTime()))
                && this.dateTimeToValidate.isBefore(recurrenceDate.atTime(this.rbacDateTimeConfig.getEndTime()))) {
            return true;
        }

        //Time to validate is outside the start and end time ranges.
        return false;
    }

    /**
     * Returns all valid days of the week that 'weekDay' belongs to.
     * First day of week is monday.
     * @return set of valid days belonging to the week of 'weekDay'.
     */
    private Set<LocalDate> getAllValidWeekdays(LocalDate weekDay, Set<DayOfWeek> validDaysOfWeek) {
        Set<LocalDate> weekDays = new LinkedHashSet<>();

        int dayOfWeekValue = weekDay.getDayOfWeek().getValue();
        LocalDate firstDayOfWeek = weekDay.minusDays(dayOfWeekValue - 1);

        //Iterate currentWeekDay between 0 and 7 and weekdays to the set.
        for(int currentDay = 0; currentDay < 7; currentDay ++) {
            LocalDate currentDate = firstDayOfWeek.plusDays(currentDay);

            if(!currentDate.isBefore(rbacDateTimeConfig.getStartDate())
                    && !currentDate.isAfter(calculateEndDate(rbacDateTimeConfig))) {
                if(validDaysOfWeek.stream().anyMatch(day -> day.getValue() == currentDate.getDayOfWeek().getValue()))
                    weekDays.add(currentDate);
            }
        }

        return weekDays;
    }

    /**
     * Calculate the end date for the given configuration.
     * @param rbacDateTimeConfig the rbac date time configuration.
     * @return the calculated end date.
     */
    private LocalDate calculateEndDate(RbacDateTimeConfig rbacDateTimeConfig) {
        RbacRecurrenceConfig rbacRecurrenceConfig = rbacDateTimeConfig.getRbacRecurrenceConfig();
        rbacRecurrenceConfig.validate();

        //Never ending scenario.
        if(rbacRecurrenceConfig.isNeverEnding())
            return LocalDate.MAX;

        //Ending with a end date.
        if(rbacRecurrenceConfig.getEndDate() != null)
            return rbacRecurrenceConfig.getEndDate();

        //Ending with recurrence count. calculate the end date based on Recurrence type.
        if(rbacRecurrenceConfig.getRbacRecurrenceType() == RbacRecurrenceType.DAYS)
            return rbacDateTimeConfig.getStartDate().plusDays(rbacRecurrenceConfig.getRepeatEvery() * rbacRecurrenceConfig.getRecurrenceCount()).minusDays(1);

        if(rbacRecurrenceConfig.getRbacRecurrenceType() == RbacRecurrenceType.WEEKS)
            return rbacDateTimeConfig.getStartDate().plusWeeks(rbacRecurrenceConfig.getRepeatEvery() * rbacRecurrenceConfig.getRecurrenceCount()).minusDays(1);

        if(rbacRecurrenceConfig.getRbacRecurrenceType() == RbacRecurrenceType.MONTHS)
            return rbacDateTimeConfig.getStartDate().plusMonths(rbacRecurrenceConfig.getRepeatEvery() * rbacRecurrenceConfig.getRecurrenceCount()).minusDays(1);

        if(rbacRecurrenceConfig.getRbacRecurrenceType() == RbacRecurrenceType.YEARS)
            return rbacDateTimeConfig.getStartDate().plusYears(rbacRecurrenceConfig.getRepeatEvery() * rbacRecurrenceConfig.getRecurrenceCount()).minusDays(1);

        //Should never hit.
        return null;
    }

    /**
     * Calculate the start date time fo the given configuration.
     * @param rbacDateTimeConfig the rbac date time configuration.
     * @return the calculated start date time.
     */
    private LocalDateTime calculateStartDateTime(RbacDateTimeConfig rbacDateTimeConfig) {
        if(rbacDateTimeConfig.isAllDay())
            return rbacDateTimeConfig.getStartDate().atStartOfDay();

        return rbacDateTimeConfig.getStartDate().atTime(rbacDateTimeConfig.getStartTime());
    }

    /**
     * Calculate the end date time for the given configuration.
     * @param rbacDateTimeConfig the rbac date time configuration.
     * @return the calculated end date time.
     */
    private LocalDateTime calculateEndDateTime(RbacDateTimeConfig rbacDateTimeConfig) {
        //Non recurring.
        if(!rbacDateTimeConfig.isRecurring()) {
            if(rbacDateTimeConfig.isAllDay()) {
                return rbacDateTimeConfig.getStartDate().atTime(LocalTime.MAX);
            }
            return rbacDateTimeConfig.getStartDate().atTime(rbacDateTimeConfig.getEndTime());
        }

        //Recurring.
        if(rbacDateTimeConfig.isAllDay())
            return calculateEndDate(rbacDateTimeConfig).atTime(LocalTime.MAX);

        return calculateEndDate(rbacDateTimeConfig).atTime(rbacDateTimeConfig.getEndTime());
    }
}
