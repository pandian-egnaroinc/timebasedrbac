package parser;

import akka.util.ByteString;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.F;
import play.libs.streams.Accumulator;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import timeBasedRbac.RbacDateTimeConfig;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.concurrent.Executor;

/**
 * Body parser for RBAC date time config.
 */
public class RbacDateTimeConfigBodyParser implements BodyParser<RbacDateTimeConfig> {
    private BodyParser.Json jsonParser;
    private Executor executor;

    @Inject
    public RbacDateTimeConfigBodyParser(BodyParser.Json jsonParser, Executor executor) {
        this.jsonParser = jsonParser;
        this.executor = executor;
    }

    @Override
    public Accumulator<ByteString, F.Either<Result, RbacDateTimeConfig>> apply(Http.RequestHeader request) {
        Accumulator<ByteString, F.Either<Result, JsonNode>> jsonAccumulator = jsonParser.apply(request);
        return jsonAccumulator.map(resultOrJson -> {
            if (resultOrJson.left.isPresent()) {
                return F.Either.Left(resultOrJson.left.get());
            } else {
                JsonNode json = resultOrJson.right.get();
                try {
                    RbacDateTimeConfig dateTimeConfig = play.libs.Json.fromJson(json, RbacDateTimeConfig.class);
                    if (dateTimeConfig.getStartDate() == null || dateTimeConfig.getStartDate().isEqual(LocalDate.MIN)) {
                        return F.Either.Left(Results.badRequest("Invalid start date"));
                    } else if (dateTimeConfig.isAllDay() == false && (dateTimeConfig.getStartTime() == null || dateTimeConfig.getEndTime() == null)) {
                        return F.Either.Left(Results.badRequest("Invalid allDay or start date or end date configuration."));
                    } else if(dateTimeConfig.isRecurring() == true && dateTimeConfig.getRbacRecurrenceConfig() == null) {
                        return F.Either.Left(Results.badRequest("No Recurrence Config found."));
                    } else if(dateTimeConfig.isRecurring() == false && dateTimeConfig.getRbacRecurrenceConfig() != null) {
                        return F.Either.Left(Results.badRequest("Invalid isRecurring value."));
                    } else {
                        return F.Either.Right(dateTimeConfig);
                    }
                } catch (Exception e) {
                    return F.Either.Left(Results.badRequest(
                            "Unable to read RBAC DateTime config info from json: " + e.getMessage()));
                }
            }
        }, executor);
    }
}